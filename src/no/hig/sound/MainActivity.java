package no.hig.sound;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Simple demo of how to generate sound programatically.
 * Check blog posts:
 * <itemize> 
 * <item>http://buildsmartrobots.ning.com/profiles/blogs/generating-audio-tones-using-android</item>
 * <item>http://marblemice.blogspot.no/2010/04/generate-and-play-tone-in-android.html</item>
 * <itemize>
 * 
 * 
 * @author mariusz
 */
public class MainActivity extends Activity {

	private final int SampleRate = 8000;
	private final int DEFAULT_DURATION = 3;
	
	private final double A_snd = 440f;
	private final double B_snd = 494f;
	private final double C_snd = 523f;
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final Button btnSoundA = (Button) findViewById(R.id.btn_sound_a);
		setupButton(btnSoundA, A_snd, DEFAULT_DURATION);
		final Button btnSoundB = (Button) findViewById(R.id.btn_sound_b);
		setupButton(btnSoundB, B_snd, DEFAULT_DURATION);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void setupButton(final Button b, final double frequency, final int time) {
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new Thread(new Runnable(){
					@Override
					public void run() {
						playSound(frequency, time);			
					}
				}).start();
			}
		});
		

	}
	
	/**
	 * Play a given sound for a given time.
	 * @param frequency frequency of sound
	 * @param duration duration of sound
	 */
	private void playSound(final double frequency, final int duration) {
		final int numSamples = duration * SampleRate;
		final double sample[] = new double[numSamples];
		final byte generatedSnd[] = new byte[2 * numSamples];
		// fill out the array
		for (int i = 0; i < numSamples; ++i) {
			sample[i] = Math.sin(2 * Math.PI * i / (SampleRate / frequency));
		}
		// convert to 16 bit pcm sound array
		// assumes the sample buffer is normalised.
		int idx = 0;
		for (double dVal : sample) {
			short val = (short) (dVal * 32767);
			generatedSnd[idx++] = (byte) (val & 0x00ff);
			generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		}
		final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
				8000, AudioFormat.CHANNEL_CONFIGURATION_MONO,
				AudioFormat.ENCODING_PCM_16BIT, numSamples,
				AudioTrack.MODE_STATIC);
		audioTrack.write(generatedSnd, 0, numSamples);
		audioTrack.play();
	}	
	
}
